﻿using ClassLibrary1.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public interface IPeopleService
    {
        void AddPerson(Person person);

        List<Person> GetPeople();
    }
}
