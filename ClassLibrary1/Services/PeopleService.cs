﻿using ClassLibrary1.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class PeopleService : IPeopleService
    {

        List<Person> ListOfPeople = new List<Person>() { new Person() { Name = "Person1", Surname = "Person1" }, new Person() { Name = "Person2", Surname = "Person2" } };
        public void AddPerson(Person person) => ListOfPeople.Add(person);

        public List<Person> GetPeople() => ListOfPeople;
    }
}
