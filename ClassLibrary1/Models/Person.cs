﻿using System;

namespace ClassLibrary1.Models
{
    public class Person
    {
        public string Name { get; set; }

        public string Surname { get; set; }
    }
}
